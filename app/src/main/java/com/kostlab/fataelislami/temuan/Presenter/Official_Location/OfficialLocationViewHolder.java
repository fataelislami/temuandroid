package com.kostlab.fataelislami.temuan.Presenter.Official_Location;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kostlab.fataelislami.temuan.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfficialLocationViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.txtLokasi)
    TextView txtLokasi;
    @BindView(R.id.txtJarak)
    TextView txtJarak;
    private ArrayList<String> officialLocationList=new ArrayList<String>();//buat array kosong

    public OfficialLocationViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
    public void setItem(ArrayList<String> item){//penerima value dari adapter
        officialLocationList=item;
    }

}

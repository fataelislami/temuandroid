package com.kostlab.fataelislami.temuan.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kostlab.fataelislami.temuan.Models.MRegister;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String item;
    @BindView(R.id.regBtn) Button regBtn;
    @BindView(R.id.regUserid) EditText regUserid;
    @BindView(R.id.regName) EditText regName;
    @BindView(R.id.regCity) EditText regCity;
    @BindView(R.id.regPassword) EditText regPasword;
    @BindView(R.id.regPhone) EditText regPhone;
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Spinner spinner = (Spinner) findViewById(R.id.regJeniskelamin);
        List<String> gender = new ArrayList<String>();
        gender.add("Laki-Laki");
        gender.add("Perempuan");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gender);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.regBtn)
    public void regBtn(){
        String id_user=regUserid.getText().toString();
        String password=regPasword.getText().toString();
        String name=regName.getText().toString();
        String city=regCity.getText().toString();
        String phone=regPhone.getText().toString();
        String gender=item;
        RestAPI service = ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call <MRegister> call=service.register(id_user,password,name,city,gender,phone);
        call.enqueue(new Callback<MRegister>() {
            @Override
            public void onResponse(Call<MRegister> call, Response<MRegister> response) {
                String message=response.body().getStatus();
                String data="Data: " + id_user+" "+password +" "+name+" "+city+" "+phone+" "+gender;
                Log.d("Data",data);
                Toast.makeText(getApplicationContext(), "Status: " + message, Toast.LENGTH_LONG).show();
                if(!message.equals("fail")){
                    Intent i=new Intent(getApplicationContext(),Login.class);
                    startActivity(i);
                }

            }

            @Override
            public void onFailure(Call<MRegister> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        item = parent.getItemAtPosition(position).toString();
        if(item.equals("Laki-Laki")){
            item="male";
        }else if(item.equals("Perempuan")){
            item="female";
        }
        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

package com.kostlab.fataelislami.temuan.Presenter.Official_Location;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kostlab.fataelislami.temuan.Models.ResultOfficialLocation;
import com.kostlab.fataelislami.temuan.R;

import java.util.ArrayList;
import java.util.List;

public class OfficialLocationAdapter extends RecyclerView.Adapter<OfficialLocationViewHolder>{
    private static final String TAG = OfficialLocationAdapter.class.getSimpleName();
    private Context context;
    private List<ResultOfficialLocation> officialLocationList;
    private ArrayList<String> newOfficialLocation=new ArrayList<String>();


    public OfficialLocationAdapter(Context context,List<ResultOfficialLocation> officialLocationList){
        this.context=context;
        this.officialLocationList=officialLocationList;
    }
    @NonNull
    @Override
    public OfficialLocationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_official_location,viewGroup,false);
        return new OfficialLocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OfficialLocationViewHolder officialLocationViewHolder, int i) {
        ResultOfficialLocation officialLocationObj=officialLocationList.get(i);
        officialLocationViewHolder.txtLokasi.setText(officialLocationObj.getName());
        officialLocationViewHolder.txtJarak.setText(Math.round(Float.parseFloat(officialLocationObj.getDistance()))+" km");
        //siapkan value untuk dikirim ke class view holder
        newOfficialLocation.add(officialLocationObj.getName()+officialLocationObj.getDistance());
        officialLocationViewHolder.setItem(newOfficialLocation);
        //end
    }

    @Override
    public int getItemCount() {
        return officialLocationList.size();
    }
}

package com.kostlab.fataelislami.temuan.Views;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kostlab.fataelislami.temuan.Models.MResponseImage;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Helper.FileUtils;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImage extends AppCompatActivity {
    private static final int PICK_IMAGE = 1;
    private static final int PERMISSION_REQUEST_STORAGE = 2;
    private Uri uri;
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";

    @BindView(R.id.img_thumb)
    ImageView imgThumb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        ButterKnife.bind(this);
        Glide.with(this).load("https://islamify.id/xfile/default.png").into(imgThumb);
    }

    @OnClick(R.id.btn_choose)
    public void choose(){
        choosePhoto();
    }

    @OnClick(R.id.btn_upload_1)
    public void upload(){
        if(uri != null) {
            File file= FileUtils.getFile(getApplicationContext(),uri);
            uploadMultipart(file);
        }else{
            Toast.makeText(this, "You must choose the image", Toast.LENGTH_SHORT).show();
        }
    }

    private void choosePhoto() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        }else{
            openGallery();
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    private void uploadMultipart(File file) {
        RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("gambar",file.getName(), photoBody);
        RestAPI service= ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call<MResponseImage> call=service.uploadPhotoMultipart(photoPart);
        call.enqueue(new Callback<MResponseImage>() {
            @Override
            public void onResponse(Call<MResponseImage> call, Response<MResponseImage> response) {
                String res=response.body().getResponse();
                Log.d("Message", "onResponse: "+res);
                Toast.makeText(getApplicationContext(), res, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<MResponseImage> call, Throwable t) {
                Log.e("Message", "onError: "+t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if(data != null) {
                uri = data.getData();
                imgThumb.setImageURI(uri);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    openGallery();
                }

                return;
            }
        }
    }

}

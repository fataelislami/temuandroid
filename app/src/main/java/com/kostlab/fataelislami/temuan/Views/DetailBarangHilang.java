package com.kostlab.fataelislami.temuan.Views;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kostlab.fataelislami.temuan.Models.MBarang;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailBarangHilang extends AppCompatActivity {
    private String id_things;
    @BindView(R.id.dbh_gmrGede)
    ImageView dbh_gmrGede;
    @BindView(R.id.dbh_btnRequest)
    Button dbh_btnRequest;
    @BindView(R.id.dbh_gmrKecil) ImageView dbh_gmrKecil;
    @BindView(R.id.dbh_Deskripsi)
    TextView dbh_Deskripsi;
    @BindView(R.id.dbh_Judul)
    TextView dbh_Judul;
    String phone;
    private static final String ROOT_URL="https://islamify.id/bots/temuan/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang_hilang);
        ButterKnife.bind(this);
        Intent i=getIntent();
        Bundle b=i.getExtras();
        if(b!=null)
        {
            id_things =(String) b.get("id_things");
            getdata(id_things);
            Log.d("Variable", "Id Things: "+id_things);
        }
    }

    private void getdata(String id_things){
        Log.d("Variable", "Id Things: "+id_things);
        RestAPI service= ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call<MBarang> call=service.getdetailbarang(id_things);
        call.enqueue(new Callback<MBarang>() {
            @Override
            public void onResponse(Call<MBarang> call, Response<MBarang> response) {
                dbh_Judul.setText(response.body().getResults().get(0).getName());
                dbh_Deskripsi.setText(response.body().getResults().get(0).getDesc());
                Glide.with(getApplicationContext())
                        .load(response.body().getResults().get(0).getImage())
                        .apply(new RequestOptions().centerCrop())
                        .into(dbh_gmrGede);
                Glide.with(getApplicationContext())
                        .load(response.body().getResults().get(0).getImage())
                        .apply(new RequestOptions().centerCrop())
                        .into(dbh_gmrKecil);
                phone=response.body().getResults().get(0).getPhone();
            }

            @Override
            public void onFailure(Call<MBarang> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.dbh_btnPanggil)
    public void panggil(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone));
        startActivity(intent);
    }
}

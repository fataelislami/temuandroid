package com.kostlab.fataelislami.temuan.Presenter;

import com.kostlab.fataelislami.temuan.Models.MBarang;
import com.kostlab.fataelislami.temuan.Models.MLogin;
import com.kostlab.fataelislami.temuan.Models.MOfficialLocation;
import com.kostlab.fataelislami.temuan.Models.MRegister;
import com.kostlab.fataelislami.temuan.Models.MResponse;
import com.kostlab.fataelislami.temuan.Models.MResponseImage;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RestAPI {
//        @GET("praytimes/get?lat=-6.914744&lng=107.609810&timezone=7")
//        Call <MPraytimes> getprayer();
        @GET("get_near_things?radius=4&id_user=U4d764405f14220a7951fc5fab795495a")
        Call <MBarang> getbarang(@Query("lat") String lat, @Query("lng") String lng,
                                 @Query("status") String status);

        @GET("laporan/byid")
        Call <MBarang> getdetailbarang(@Query("id") String id);

        @GET("barang/terbaru")
        Call <MBarang> getterbaru();

        @GET("official_location?radius=4")
        Call <MOfficialLocation> getlocation(@Query("lat") String lat, @Query("lng") String lng);



        @FormUrlEncoded
        @POST("register/proses")
        Call<MRegister> register(@Field("id") String id_user, @Field("password") String password,
                                 @Field("name") String name,@Field("city") String city,
                                 @Field("gender") String gender,@Field("phone") String phone);

        @FormUrlEncoded
        @POST("login/auth")
        Call<MLogin> login(@Field("id") String id_user,@Field("password") String password);

        @Multipart
        @POST("upload/image")
        Call<MResponseImage> uploadPhotoMultipart(@Part MultipartBody.Part file);

        @Multipart
        @POST("laporan/create")
        Call<MResponse> uploadLaporan(@Part MultipartBody.Part file,@Part("id_user") RequestBody id_user
                                        ,@Part("name") RequestBody name,@Part("desc") RequestBody desc
                                        ,@Part("datetime") RequestBody datetime,@Part("lat") RequestBody lat,
                                      @Part("lng") RequestBody lng,@Part("status") RequestBody status);
}

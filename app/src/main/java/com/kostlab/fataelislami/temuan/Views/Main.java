package com.kostlab.fataelislami.temuan.Views;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.kostlab.fataelislami.temuan.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main extends AppCompatActivity {
//Kalo mau pake butter knife deklarasi dulu disini setiap item view yang pake id nya
//START DECLARATION
@BindView(R.id.navigation) BottomNavigationView navigation;
//END OF DECLARATION
//    private ActionBar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);//jangan lupa cantumkan ini setiap mau pake butter knife
//        toolbar =getSupportActionBar();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new Home());
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_homepage:
//                    toolbar.setTitle("Main");
                    loadFragment(new Home());
                    return true;
                case R.id.navigation_location:
//                    toolbar.setTitle("ListLocation");
                    loadFragment(new ListLocation());
                    return true;
                case R.id.navigation_myreport:
//                    toolbar.setTitle("My Report");
                    loadFragment(new LaporanSaya());
                    return true;
                case R.id.navigation_notification:
//                    toolbar.setTitle("Notification");
                    loadFragment(new Notification());
                    return true;
                case R.id.navigation_profile:
//                    toolbar.setTitle("Profile");
                    loadFragment(new Profile());
                    return true;
            }
            return false;
        }
    };


    void loadFragment(Fragment fragment){
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout,fragment);
        ft.commit();
    }
}

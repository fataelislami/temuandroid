package com.kostlab.fataelislami.temuan.Views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kostlab.fataelislami.temuan.Models.MLogin;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    @BindView(R.id.logBtnlogin)
    Button logBtnlogin;
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";
    @BindView(R.id.logUsername)
    EditText logUsername;
    @BindView(R.id.logPassword)
    EditText logPassword;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AuthLogin", 0);
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    @OnClick(R.id.logBtnlogin)
    public void auth(){
        String username=logUsername.getText().toString();
        String password=logPassword.getText().toString();
        RestAPI service= ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call <MLogin> call=service.login(username,password);
        call.enqueue(new Callback<MLogin>() {
            @Override
            public void onResponse(Call<MLogin> call, Response<MLogin> response) {
                Integer loginStatus=response.body().getLogin();//ambil nilai login ke API
                if(loginStatus!=0){
                    String id_user=response.body().getId_user();
                    String name=response.body().getName();
                    String city=response.body().getCity();
                    String gender=response.body().getGender();
                    String phone=response.body().getPhone();
                    editor.putString("status", "login"); // Storing string
                    editor.putString("id_user",id_user);
                    editor.putString("name",name);
                    editor.putString("city",city);
                    editor.putString("gender",gender);
                    editor.putString("phone",phone);
                    editor.commit();
                    Intent i=new Intent(getApplicationContext(),Main.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<MLogin> call, Throwable t) {

            }
        });


    }

    @OnClick(R.id.logBtnregister)
    public void logBtnregister(){
        Intent i=new Intent(getApplicationContext(),Register.class);
        startActivity(i);
    }
}

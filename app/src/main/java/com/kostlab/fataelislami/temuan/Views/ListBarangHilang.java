package com.kostlab.fataelislami.temuan.Views;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kostlab.fataelislami.temuan.Helper.Gps;
import com.kostlab.fataelislami.temuan.Models.MBarang;
import com.kostlab.fataelislami.temuan.Presenter.Barang_hilang.BarangHilangAdapter;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListBarangHilang extends AppCompatActivity {
    //Kalo mau pake butter knife deklarasi dulu disini setiap item view yang pake id nya
    //START DECLARATION
    @BindView(R.id.barang_hilang_list) RecyclerView barang_list;
    //END OF DECLARATION
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_barang_hilang);
        ButterKnife.bind(this);
        Gps gps = new Gps(this);
                if(gps.canGetLocation()){
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                System.out.println("Your ListLocation is - \nLat: " + latitude + "\nLong: " + longitude);
                getdata(latitude,longitude);
                }else{
                gps.showSettingsAlert();
                }

    }

    @OnClick(R.id.changeGrid)
    public void changeGrid(){
        GridLayoutManager mGrid = new GridLayoutManager(getApplicationContext(), 2);
        barang_list.setLayoutManager(mGrid);
    }
    @OnClick(R.id.changeLinear)
    public void changeLinear(){
        LinearLayoutManager mLinear = new LinearLayoutManager(getApplicationContext());
        barang_list.setLayoutManager(mLinear);
    }

    public void getdata(double latAPI,double lngAPI) {//get data dari api
        //params &lat=-6.886863&lng=107.615311&status=lost&
        RestAPI service = ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call<MBarang> call = service.getbarang(String.valueOf(latAPI), String.valueOf(lngAPI), "lost");
        // Nambahin Loading pas lagi get data
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(ListBarangHilang.this);
        progressDialog.setMax(100);
        progressDialog.setMessage("Mengambil Data....");
        progressDialog.setTitle("Mohon Bersabar");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // Munculin Loading
        progressDialog.show();
        call.enqueue(new Callback<MBarang>() {
            @Override
            public void onResponse(Call<MBarang> call, Response<MBarang> response) {
                if(response.body().getResults()!=null) {
                    LinearLayoutManager mLinear = new LinearLayoutManager(getApplicationContext());
                    GridLayoutManager mGrid = new GridLayoutManager(getApplicationContext(), 2);
                    barang_list.setLayoutManager(mLinear);
                    barang_list.setHasFixedSize(true);
                    //Mengambil nilai dari response dan dikirim ke adapter
                    BarangHilangAdapter barangAdapter = new BarangHilangAdapter(ListBarangHilang.this, response.body().getResults());
                    barang_list.setAdapter(barangAdapter);
                }
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<MBarang> call, Throwable t) {
                progressDialog.dismiss();

            }
        });
    }
}


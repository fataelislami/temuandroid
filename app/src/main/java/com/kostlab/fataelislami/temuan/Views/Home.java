package com.kostlab.fataelislami.temuan.Views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kostlab.fataelislami.temuan.Models.MBarang;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Home.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends Fragment {
    //Start Declaration
    @BindView(R.id.imgBarangTemuan)
    ImageView imgBarangTemuan;
    @BindView(R.id.imgBarangHilang)
    ImageView imgBarangHilang;
    @BindView(R.id.vector_statistic)
    ImageView vector_statistic;
    @BindView(R.id.cvImg1) ImageView cvImg1;
    @BindView(R.id.cvImg2) ImageView cvImg2;
    @BindView(R.id.cvImg3) ImageView cvImg3;
    @BindView(R.id.cvImg4) ImageView cvImg4;
    @BindView(R.id.cvName1) TextView cvName1;
    @BindView(R.id.cvName2) TextView cvName2;
    @BindView(R.id.cvName3) TextView cvName3;
    @BindView(R.id.cvName4) TextView cvName4;
    String id_things1;
    String id_things2;
    String id_things3;
    String id_things4;


    private static final String ROOT_URL="https://islamify.id/bots/temuan/api/";

    //
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home.
     */
    // TODO: Rename and change types and number of parameters
    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        getdata();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    //Untuk membuat event klik dari butter knife gunakan @OnCLick(R.id.namaidview) terus kasih void klik()
    @OnClick(R.id.imgBarangHilang)
    public void imgBarangHilang(){
        Intent i=new Intent(getContext(),ListBarangHilang.class);
        startActivity(i);
    }

    @OnClick(R.id.imgBarangTemuan)
    public void imgBarangTemuan(){
        Intent i=new Intent(getContext(),ListBarangTemuan.class);
        startActivity(i);
    }

    @OnClick(R.id.imgBuatLaporan)
    public void imgBuatLaporan(){
        Intent i=new Intent(getContext(),FormLaporan.class);
        startActivity(i);
    }

    public void getdata(){
        RestAPI service= ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call<MBarang> call=service.getterbaru();
        call.enqueue(new Callback<MBarang>() {
            @Override
            public void onResponse(Call<MBarang> call, Response<MBarang> response) {
                    cvName1.setText(response.body().getResults().get(0).getName());
                    Glide.with(getContext()).load(response.body().getResults().get(0).getImage()).apply(new RequestOptions().centerCrop()).into(cvImg1);
                cvName2.setText(response.body().getResults().get(1).getName());
                Glide.with(getContext()).load(response.body().getResults().get(1).getImage()).apply(new RequestOptions().centerCrop()).into(cvImg2);
                cvName3.setText(response.body().getResults().get(2).getName());
                Glide.with(getContext()).load(response.body().getResults().get(2).getImage()).apply(new RequestOptions().centerCrop()).into(cvImg3);
                cvName4.setText(response.body().getResults().get(3).getName());
                Glide.with(getContext()).load(response.body().getResults().get(3).getImage()).apply(new RequestOptions().centerCrop()).into(cvImg4);

                id_things1=response.body().getResults().get(0).getId_things();
                id_things2=response.body().getResults().get(1).getId_things();
                id_things3=response.body().getResults().get(2).getId_things();
                id_things4=response.body().getResults().get(3).getId_things();

            }

            @Override
            public void onFailure(Call<MBarang> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.h_cardView1)
    public void pindah1(){
        Intent i =new Intent(getContext(), DetailBarangTemuan.class);
        i.putExtra("id_things",id_things1);
        startActivity(i);
    }
    @OnClick(R.id.h_cardView2)
    public void pindah2(){
        Intent i =new Intent(getContext(), DetailBarangTemuan.class);
        i.putExtra("id_things",id_things2);
        startActivity(i);
    }
    @OnClick(R.id.h_cardView3)
    public void pindah3(){
        Intent i =new Intent(getContext(), DetailBarangTemuan.class);
        i.putExtra("id_things",id_things3);
        startActivity(i);
    }
    @OnClick(R.id.h_cardView4)
    public void pindah4(){
        Intent i =new Intent(getContext(), DetailBarangTemuan.class);
        i.putExtra("id_things",id_things4);
        startActivity(i);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

package com.kostlab.fataelislami.temuan.Presenter.Barang_hilang;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kostlab.fataelislami.temuan.Models.ResultBarang;
import com.kostlab.fataelislami.temuan.R;

import java.util.ArrayList;
import java.util.List;

public class BarangHilangAdapter extends RecyclerView.Adapter<BarangHilangViewHolder>{
    private static final String TAG = BarangHilangAdapter.class.getSimpleName();
    private Context context;
    private List<ResultBarang> barangList;
    private ArrayList<String> newBarang=new ArrayList<String>();

    public BarangHilangAdapter(Context context, List<ResultBarang> barangList){
        this.context=context;
        this.barangList=barangList;
    }
    @NonNull
    @Override
    public BarangHilangViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_barang_hilang,viewGroup,false);
        return new BarangHilangViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangHilangViewHolder barangHilangViewHolder, int i) {
    ResultBarang barangObject=barangList.get(i);
    barangHilangViewHolder.txtBarang.setText(barangObject.getName());
    barangHilangViewHolder.txtDescBarang.setText(barangObject.getDesc());
    //Play Glide
        Glide.with(context)
                .load(barangObject.getImage())
                .apply(new RequestOptions().override(100,100).centerCrop())
                .into(barangHilangViewHolder.gmrBarangHilang);
    //Play Glide END
    //siapkan value untuk dikirim ke class view holder
    newBarang.add(barangObject.getId_things());
    barangHilangViewHolder.setItem(newBarang);
    //end
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }
}

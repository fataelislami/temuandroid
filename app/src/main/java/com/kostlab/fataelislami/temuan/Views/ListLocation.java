package com.kostlab.fataelislami.temuan.Views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kostlab.fataelislami.temuan.Helper.Gps;
import com.kostlab.fataelislami.temuan.Models.MOfficialLocation;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.Official_Location.OfficialLocationAdapter;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListLocation.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListLocation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListLocation extends Fragment implements OnMapReadyCallback {
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";

    private GoogleMap mMap;
    @BindView(R.id.location_list)
    RecyclerView location_list;
    Gps gps;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListLocation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListLocation.
     */
    // TODO: Rename and change types and number of parameters
    public static ListLocation newInstance(String param1, String param2) {
        ListLocation fragment = new ListLocation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        ButterKnife.bind(this, view);

        SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
//        Gps gps = new Gps(getContext());
//        String latlong=gps.getLatitude()+" | "+gps.getLongitude();
//        txtLatLong.setText(latlong);
////        Toast.makeText(getContext(),gps.getLatitude(),Toast.LENGTH_LONG);

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("On Resume");

    }
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gps = new Gps(getContext());
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            // \n is for new line
            System.out.println("Your ListLocation is - \nLat: " + latitude + "\nLong: " + longitude);
            mMap = googleMap;
            // Add a marker in Unikom and move the camera
//            LatLng unikom = new LatLng(-6.886579, 107.615);
            LatLng mylocation = new LatLng(latitude, longitude);
//            mMap.addMarker(new MarkerOptions().position(unikom).title("Marker in Unikom"));
//            mMap.addMarker(new MarkerOptions().position(mylocation).title("Ini Kamu Cuy").icon(BitmapDescriptorFactory.fromResource(R.drawable.hero)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 17));
            getdata(latitude,longitude,googleMap);
            mMap.setMyLocationEnabled(true);

        }else{
            gps.showSettingsAlert();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getdata(Double lat,Double lng,GoogleMap googleMap){
        RestAPI service=ClientAPI.get(ROOT_URL).create(RestAPI.class);
        Call<MOfficialLocation> call=service.getlocation(lat.toString(), lng.toString());
        // Nambahin Loading pas lagi get data
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMax(100);
        progressDialog.setMessage("Mengambil Data....");
        progressDialog.setTitle("Mohon Bersabar");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // Munculin Loading
        progressDialog.show();
        call.enqueue(new Callback<MOfficialLocation>() {
            @Override
            public void onResponse(Call<MOfficialLocation> call, Response<MOfficialLocation> response) {
                mMap=googleMap;
                if(response.body().getResults()!=null){
                    for (int i=0;i<response.body().getResults().size();i++){
                        Double latAPI=Double.parseDouble(response.body().getResults().get(i).getLatitude());
                        Double lngAPI=Double.parseDouble(response.body().getResults().get(i).getLongitude());
                        mMap.addMarker(new MarkerOptions().position(new LatLng(latAPI,lngAPI)).title(response.body().getResults().get(i).getName()));
                    }
                    LinearLayoutManager mLinear = new LinearLayoutManager(getContext());
                    GridLayoutManager mGrid = new GridLayoutManager(getContext(), 2);
                    location_list.setLayoutManager(mLinear);
                    location_list.setHasFixedSize(true);
                    //Mengambil data dari adapter
                    OfficialLocationAdapter officialLocationAdapter=new OfficialLocationAdapter(getContext(),response.body().getResults());
                    location_list.setAdapter(officialLocationAdapter);
                }
//                Log.d("Result API", response.body().getTotalResult().toString());
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MOfficialLocation> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}

package com.kostlab.fataelislami.temuan.Presenter.Barang_hilang;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kostlab.fataelislami.temuan.R;
import com.kostlab.fataelislami.temuan.Views.DetailBarangHilang;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class BarangHilangViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.txtBarangHilang)
    TextView txtBarang;
    @BindView(R.id.txtDescBarangHilang) TextView txtDescBarang;
    @BindView(R.id.gmrBarangHilang) ImageView gmrBarangHilang;
    private ArrayList<String> barangList=new ArrayList<String>();//buat array kosong
    public BarangHilangViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    public void setItem(ArrayList<String> item){//penerima value dari adapter
        barangList=item;
    }

    @OnClick()
    void onClick(@NonNull View itemView){
        int position =getAdapterPosition();
        Intent i =new Intent(itemView.getContext(), DetailBarangHilang.class);
        i.putExtra("id_things",barangList.get(position));
        itemView.getContext().startActivity(i);
            Toast.makeText(itemView.getContext(),"Nama "+barangList.get(position)+position,Toast.LENGTH_LONG).show();
    }
}

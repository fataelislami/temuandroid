
package com.kostlab.fataelislami.temuan.Models;

import java.util.HashMap;
import java.util.Map;

public class ResultBarang {

    private String id_things;
    private String name;
    private String desc;
    private String datetime;
    private String latitude;
    private String longitude;
    private String verification;
    private String image;
    private String status;
    private String id_admin;
    private String id_user;
    private String phone;
    private Object id_official_location;
    private Object username_official_location;
    private Object official_location;
    private Object id_user2;
    private Object visible;
    private String insert_time;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId_things() {
        return id_things;
    }

    public void setId_things(String id_things) {
        this.id_things = id_things;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone=phone;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_admin() {
        return id_admin;
    }

    public void setId_admin(String id_admin) {
        this.id_admin = id_admin;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Object getId_official_location() {
        return id_official_location;
    }

    public void setId_official_location(Object id_official_location) {
        this.id_official_location = id_official_location;
    }

    public Object getUsername_official_location() {
        return username_official_location;
    }

    public void setUsername_official_location(Object username_official_location) {
        this.username_official_location = username_official_location;
    }

    public Object getOfficial_location() {
        return official_location;
    }

    public void setOfficial_location(Object official_location) {
        this.official_location = official_location;
    }

    public Object getId_user2() {
        return id_user2;
    }

    public void setId_user2(Object id_user2) {
        this.id_user2 = id_user2;
    }

    public Object getVisible() {
        return visible;
    }

    public void setVisible(Object visible) {
        this.visible = visible;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

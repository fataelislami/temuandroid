package com.kostlab.fataelislami.temuan.Views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kostlab.fataelislami.temuan.Helper.FileUtils;
import com.kostlab.fataelislami.temuan.Helper.Gps;
import com.kostlab.fataelislami.temuan.Models.MResponse;
import com.kostlab.fataelislami.temuan.Presenter.ClientAPI;
import com.kostlab.fataelislami.temuan.Presenter.RestAPI;
import com.kostlab.fataelislami.temuan.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormLaporan extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @BindView(R.id.fl_Simpan)
    Button fl_simpan;
    @BindView(R.id.fl_Nama) EditText fl_nama;
    @BindView(R.id.fl_Deskripsi) EditText fl_deskripsi;
    @BindView(R.id.fl_latitude) EditText fl_latitude;
    @BindView(R.id.fl_longitude) EditText fl_longitude;
    @BindView(R.id.fl_pilihGambar) Button fl_pilihGambar;
    @BindView(R.id.fl_imageThumb)
    ImageView fl_imageThumb;
    private static final String TAG = "FormLaporan";
    private EditText mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    Calendar calendar = Calendar.getInstance();
    int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
    int currentMinute = calendar.get(Calendar.MINUTE);
    String date;
    String time;
    String tipeLaporan;
    String id_user;
    String nama;
    String deskripsi;
    String latitude;
    String longitude;
    String datetime;
    String status;
    private static final int PICK_IMAGE = 1;
    private static final int PERMISSION_REQUEST_STORAGE = 2;
    private Uri uri;
    Gps gps;
    public static final String ROOT_URL = "https://islamify.id/bots/temuan/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_laporan);
        ButterKnife.bind(this);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AuthLogin", 0); // 0 - for private mode
        String statusLogin=pref.getString("status",null);
        if(statusLogin==null){
            Intent i=new Intent(this,Login.class);
            startActivity(i);
        }
        id_user=pref.getString("id_user",null);
        Log.d(TAG, "LoginStatus: "+statusLogin);
        Log.d(TAG, "IdUser: "+pref.getString("id_user",null));
        Log.d(TAG, "Name: "+pref.getString("name",null));
        gps = new Gps(getApplicationContext());
                if(gps.canGetLocation()){
                    try {
                        double Glatitude = gps.getLatitude();
                        double Glongitude = gps.getLongitude();
                        fl_latitude.setText(String.valueOf(Glatitude));
                        fl_longitude.setText(String.valueOf(Glongitude));
                        Log.d(TAG, "Value: "+Glatitude+"LNG:"+Glongitude);
                    }catch (Exception e){

                    }
                }else{
                gps.showSettingsAlert();
                }
        Spinner spinner = (Spinner) findViewById(R.id.typeBarang);
        List<String> typeBarang = new ArrayList<String>();
        typeBarang.add("Barang Hilang");
        typeBarang.add("Barang Temuan");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typeBarang);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        //
        mDisplayDate = (EditText) findViewById(R.id.fl_Tanggal);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        FormLaporan.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = year + "-" + month + "-" + day;
                mDisplayDate.setText(date);
            }
        };
        EditText chooseTime = findViewById(R.id.fl_Waktu);
        chooseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(FormLaporan.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
                        time=hourOfDay+":"+minutes;
                        chooseTime.setText(hourOfDay + ":" + minutes);
                    }
                },currentHour,currentMinute, false);
                timePickerDialog.show();
            }

        });
    }

    @OnClick(R.id.fl_Simpan)
    public void simpan(){
        nama=fl_nama.getText().toString();
         deskripsi=fl_deskripsi.getText().toString();
         latitude=fl_latitude.getText().toString();
         longitude=fl_longitude.getText().toString();
         datetime=date+time+":00";
         status=tipeLaporan;
        if(uri != null) {
            File file= FileUtils.getFile(getApplicationContext(),uri);
            uploadMultipart(file);
        }else{
            Toast.makeText(this, "Kamu harus memilih 1 Gambar", Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, "Value"+nama+deskripsi+latitude+longitude+datetime+status);
    }

    @OnClick(R.id.fl_pilihGambar)
    public void pilihGambar(){
        choosePhoto();
    }

    private void choosePhoto() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        }else{
            openGallery();
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    private void uploadMultipart(File file) {
        RequestBody photoBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("gambar",file.getName(), photoBody);
        RestAPI service= ClientAPI.get(ROOT_URL).create(RestAPI.class);
        RequestBody rid_user=RequestBody.create(MediaType.parse("text/plain"),id_user);
        RequestBody rnama=RequestBody.create(MediaType.parse("text/plain"),nama);
        RequestBody rdeskripsi=RequestBody.create(MediaType.parse("text/plain"),deskripsi);
        RequestBody rdatetime=RequestBody.create(MediaType.parse("text/plain"),datetime);
        RequestBody rlat=RequestBody.create(MediaType.parse("text/plain"),latitude);
        RequestBody rlng=RequestBody.create(MediaType.parse("text/plain"),longitude);
        RequestBody rstatus=RequestBody.create(MediaType.parse("text/plain"),status);




        Call<MResponse> call=service.uploadLaporan(photoPart,rid_user,rnama,rdeskripsi,rdatetime,rlat,rlng,rstatus);
        call.enqueue(new Callback<MResponse>() {
            @Override
            public void onResponse(Call<MResponse> call, Response<MResponse> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                Intent i=new Intent(getApplicationContext(),Main.class);
                startActivity(i);
            }

            @Override
            public void onFailure(Call<MResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        tipeLaporan = parent.getItemAtPosition(position).toString();
        if(tipeLaporan.equals("Barang Hilang")){
            tipeLaporan="lost";
        }else if(tipeLaporan.equals("Barang Temuan")){
            tipeLaporan="found";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if(data != null) {
                uri = data.getData();
                fl_imageThumb.setImageURI(uri);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                }
                return;
            }
        }
    }
}

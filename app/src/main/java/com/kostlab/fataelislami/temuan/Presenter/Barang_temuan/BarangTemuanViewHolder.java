package com.kostlab.fataelislami.temuan.Presenter.Barang_temuan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kostlab.fataelislami.temuan.R;
import com.kostlab.fataelislami.temuan.Views.DetailBarangTemuan;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BarangTemuanViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.txtBarangTemuan)
    TextView txtBarang;
    @BindView(R.id.txtDescBarangTemuan)
    TextView txtDescBarang;
    @BindView(R.id.gmrBarangtemuan)
    ImageView gmrBarangtemuan;
    private ArrayList<String> barangList=new ArrayList<String>();//buat array kosong

    public void setItem(ArrayList<String> item){//penerima value dari adapter
        barangList=item;
    }

    public BarangTemuanViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    @OnClick()
    void onClick(@NonNull View itemView){
        int position =getAdapterPosition();
        Intent i =new Intent(itemView.getContext(), DetailBarangTemuan.class);
        i.putExtra("id_things",barangList.get(position));
        itemView.getContext().startActivity(i);
        Toast.makeText(itemView.getContext(),"Nama "+barangList.get(position)+position,Toast.LENGTH_LONG).show();
    }
}

package com.kostlab.fataelislami.temuan.Presenter.Barang_temuan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kostlab.fataelislami.temuan.Models.ResultBarang;
import com.kostlab.fataelislami.temuan.R;

import java.util.ArrayList;
import java.util.List;

public class BarangTemuanAdapter extends RecyclerView.Adapter<BarangTemuanViewHolder> {
    private Context context;
    private List<ResultBarang> barangList;
    private ArrayList<String> newBarang=new ArrayList<String>();
    public BarangTemuanAdapter(Context context,List<ResultBarang> barangList){
        this.context=context;
        this.barangList=barangList;
    }
    @NonNull
    @Override
    public BarangTemuanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(context).inflate(R.layout.item_barang_temuan,viewGroup,false);//baru sampe sini
        return new BarangTemuanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangTemuanViewHolder barangTemuanViewHolder, int i) {
        ResultBarang barangObject=barangList.get(i);
        barangTemuanViewHolder.txtBarang.setText(barangObject.getName());
        barangTemuanViewHolder.txtDescBarang.setText(barangObject.getDesc());
        Glide.with(context)
                .load(barangObject.getImage())
                .apply(new RequestOptions().override(300,300).centerCrop())
                .into(barangTemuanViewHolder.gmrBarangtemuan);
        //siapkan value untuk dikirim ke class view holder
        newBarang.add(barangObject.getId_things());
        barangTemuanViewHolder.setItem(newBarang);
        //end
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }
}
